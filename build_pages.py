#! /usr/bin/env python3
import os

PAGES = [
    {'name': 'index', 'titleSR': 'Početna', 'titleEN': 'Home', 'style': 'home'},
    {'name': 'account', 'titleSR': 'Nalog', 'titleEN': 'Account', 'style': 'account'},
    {'name': 'about', 'titleSR': 'O nama', 'titleEN': 'About us', 'style': 'about'},
    {'name': 'statute', 'titleSR': 'Statut', 'titleEN': 'Statute', 'style': 'statute'},
    {'name': 'events', 'titleSR': 'Događaji', 'titleEN': 'Events', 'style': 'events'},
    {'name': 'events_archive', 'titleSR': 'Arhiva događaja', 'titleEN': 'Events archive', 'style': 'events'},
    {'name': 'services', 'titleSR': 'Servisi', 'titleEN': 'Services', 'style': 'services'},
    {'name': 'webring', 'titleSR': 'Webring', 'titleEN': 'Webring', 'style': ''},
    {'name': 'donations', 'titleSR': 'Donacije', 'titleEN': 'Donations', 'style': 'home'},
]

def buildPage(filename: str, pageTitle: str, pageHtml: str, pageStyle: str, template: str) -> str:
    template = template.replace('<!--TITLE-->', pageTitle)
    style =  '' if not pageStyle else f'<link rel=\"stylesheet\" href=\"/styles/{pageStyle}.css\">'
    template = template.replace('<!--ADDITIONAL_STYLE-->', style)
    template = template.replace('PAGE_NAME', filename)
    template = template.replace('<!--MAIN-->', pageHtml)
    return template

def main():
    os.makedirs('site/en/', exist_ok=True)
    with open('template/page-en.html') as fTempEN, open('template/page-sr.html') as fTempSR:
        templateSR = fTempSR.read()
        templateEN = fTempEN.read()
        for page in PAGES:
            with open(f'pages/sr/{page["name"]}.html') as f:
                pageHtml = f.read()
                html = buildPage(page['name'], page['titleSR'], pageHtml, page['style'], templateSR)
                f = open(f'site/{page["name"]}.html', 'w')
                f.write(html)
                f.close()
            with open(f'pages/en/{page["name"]}.html') as f:
                pageHtml = f.read()
                html = buildPage(page['name'], page['titleEN'], pageHtml, page['style'], templateEN)
                f = open(f'site/en/{page["name"]}.html', 'w')
                f.write(html)
                f.close()

if __name__ == '__main__':
    main()

# Decentrala

Redisign of dmz.rs .

## Build site

Run

```
python atom_gen.py
python prep.py
python build_pages.py
```

Complete website will be contained in `site/`. You can copy this to server.

## Development server

To start a development server, first build site, then run (possibly with `sudo`)

```
nginx -p . -c nginx.dev.conf
```

To stop it:

```
nginx -p . -s stop
```

## TODO:

- [x] create page builder
  - rename `prep.py` to more informative name (`build_events.py`)
- [ ] create blogging system
- [ ] create xmpp bot that connects to events section.
- [ ] webring system
  - [x] make page
  - [ ] populate page
- [x] make english version
  - double check spelling and wording
- add account and donations page and style them with the site style

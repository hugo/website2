#! /usr/bin/env python3
import csv
import datetime as dt
from dateutil import relativedelta

EVENTS_CSV_PATH = "dogadjaji.csv"
CURRENT_TIME = dt.date.today()
NEXT_MONTH = CURRENT_TIME + relativedelta.relativedelta(months=1, day=1)
DAYS_OF_WEEK_SR = ("PON", "UTO", "SRE", "ČET", "PET", "SUB", "NED")
MONTHS_SR = ("Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust",\
            "Septembar", "Oktobar", "Novembar", "Decembar")

def load_events(csv_path:str) -> list[dict]:
    events = []
    with open(csv_path) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader, None)
        for event in csv_reader:
            event_date = event[0]
            event_date_parsed = dt.datetime.strptime(event_date, "%d-%m-%Y").date()
            event_time = event[1]
            event_title = event[3]
            current_event = {"date":event_date_parsed,
                             "time":event_time,
                             "title":event_title.strip()}
            if event_date_parsed >= NEXT_MONTH:
                events.append(current_event)
        return events

def render_table(events:list[dict])-> str:
    html = ""
    for event in events:
        date = DAYS_OF_WEEK_SR[event["date"].weekday()]
        day = event["date"].day
        title = event["title"]
        html += f"\t\t\t<tr> <td>{date}</td> <td>{day}.</td> <td>{title}</td> </tr>\n"
    return html

def render_page(table: str) -> str:
    head = "<head><meta charset=\"UTF-8\"><link rel=\"stylesheet\"\
href=\"styles/poster.css\"><head>"
    header = "<h1>DECENTRALA</h1>"
    subheader = f"<h2>Plan za {MONTHS_SR[NEXT_MONTH.month - 1]}</h2>"
    link = "<div id=link><img src=\"/img/logo-light.svg\"> dmz.rs</div>"
    p1 = "<p>Radionice počinju u <strong>19h</strong> u Društvenom centru Krov\
u <strong>Kraljice Marije 47</strong>.</p>"
    p2 = "<p>Ulaz u zgradu je u prolazu pored Štark prodavnice slatkiša, odmah\
pored menjačnice. DC Krov je na poslednjem spratu.</p>"
    footer = f"{p1}{p2}{link}"
    return f"<html>{head}<body><main>{header}{subheader}\
<table>{table}</table>{footer}</main></body></html>"
    
def main():
    events = load_events(EVENTS_CSV_PATH)
    table = render_table(events)
    page = render_page(table)
    f = open("poster.html", "w")
    f.write(page)
    f.close()

if __name__ == "__main__":
    main()
